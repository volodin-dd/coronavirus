import plotly.graph_objects as go
from plotly.subplots import make_subplots
import pandas as pd
from dstack import create_frame
import plotly.express as px

covid_rel_data = pd.read_csv('data/coronavirus.csv', sep='\t')
infected_data = pd.read_csv('data/infected.csv', sep='\t')

countries = covid_rel_data[covid_rel_data['type'] == 'confirmed'].groupby('Country.Region').agg({'cases': 'sum'}).sort_values('cases', ascending=False)
countries = countries.index[0:51]
types = covid_rel_data['type'].unique().tolist()

covid_long = pd.read_csv('data/coronavirus_long.csv', sep='\t')
dates = covid_long['date'].unique()

def country_dashboard(country, types):
    temp_df = covid_rel_data[covid_rel_data['Country.Region'] == country]
    temp_infected = infected_data[infected_data['Country.Region'] == country]
    fig = make_subplots(
        rows=5, cols=1,
        shared_xaxes=True,
        vertical_spacing=0.03,
        specs=[[{'type': 'table'}],
               [{'type': 'scatter'}],
               [{'type': 'scatter'}],
               [{'type': 'scatter'}],
               [{'type': 'scatter'}]]
    )

    for t in types:
        fig.add_trace(
            go.Scatter(
                x=temp_df['date'][temp_df['type'] == t],
                y=temp_df['cases'][temp_df['type'] == t],
                mode='lines',
                name=f"New {t}"
            ),
            row=2, col=1
        )

        fig.add_trace(
            go.Scatter(
                x=temp_df['date'][temp_df['type'] == t],
                y=temp_df['cum_cases'][temp_df['type'] == t],
                mode='lines',
                name=f"Cumulative {t}"
            ),
            row=3, col=1
        )

        fig.add_trace(
            go.Scatter(
                x=temp_df['date'][temp_df['type'] == t],
                y=temp_df['rel_cum_cases'][temp_df['type'] == t],
                mode='lines',
                name=f"Relative cumulative {t}"
            ),
            row=4, col=1
        )

    fig.add_trace(
        go.Table(
            header=dict(
                values=['Date', ' Case type', 'New cases',
                        'Cumulative cases', 'Relative<br>cumulative cases'],
                font=dict(size=10),
                align='left'
            ),
            cells=dict(
                values=[temp_df[k].tolist() for k in temp_df.columns[1:]],
                align='left'
            )
        ),
        row=1, col=1
    )

    fig.add_trace(
        go.Scatter(
            x=temp_infected['date'],
            y=temp_infected['infected'],
            mode='lines',
            name='Infected'
        ),
        row=5, col=1
    )

    fig.update_layout(
        height=1000,
        width=600,
        showlegend=True,
        title_text=f"{country} COVID-19 cases"
    )
    return fig

frame = create_frame("rel_covid_py")



for c in countries:
    for d in dates[::7]:
        temp_covid_long = covid_long[covid_long['Country.Region'] == c]
        fig = px.line(data_frame=temp_covid_long, x='date', y='value', color='type', facet_col='metric',
                      range_x=[d, dates.max()])
        fig.update_yaxes(matches=None)
        frame.commit(fig, f"New, cumulative and relative cumulative cases of COVID-19 by type in {c}",
                     {"Country": c, "Start date": d})

frame.push()